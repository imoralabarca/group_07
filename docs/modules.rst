functions
=========

.. toctree::
   :maxdepth: 4

   arima_method
   countries_energy_revised
   emissions_vs_consumption
   revised_area_chart
   revised_compare_gdp
   revised_gapminder
